<?php
require "vendor/autoload.php";

use DiDom\Document;

$data = [];

$url = $argv[1];

$page = 0;
while (true) {
    if ($page == 0) {
        $dataObject = new Document($url, true);
    } else {
        $dataObject = new Document($url . '&p=' . $page, true);
    }

    $pos = strpos($dataObject->text(), 'You have no results');

    if ($pos !== false) {
        break;
    }

    $documentsList[] = $dataObject;
    $page++;
}

foreach ($documentsList as $document) {
    $table = $document->find('#resultsTable');

    echo "<pre>";
    $trs = $table[0]->find('tr');
    unset($trs[0]);

    $i = 0;
    foreach ($trs as $item) {
        $data[$i]['number'] = $item->find('.number')[0]->find('a')[0]->text();

        $logo = $item->find('.trademark')[0]->find('img');
        if (isset($logo[0])) {
            $data[$i]['logo_url'] = $item->find('.trademark')[0]->find('img')[0]->getAttribute('src');
        } else {
            $data[$i]['logo_url'] = null;
        }

        $name = $item->find('.words')[0]->text();
        $name = str_replace("\n", ' ', $name);
        $name = strip_tags($name);
        $name = trim($name);
        $data[$i]['name'] = $name;

        $classes = str_replace("\n", ' ', $item->find('.classes')[0]->text());
        $data[$i]['classes'] = trim($classes);

        $status = $item->find('.status')[0];


        if (isset($status->find('.registered')[0])) {
            $status1 = 'Registered';

            if (isset($status->find('span')[0])) {
                $status2 = $status->find('span')[0]->text();
            }
        } elseif (isset($status->find('.removed')[0])) {
            $status1 = 'Removed';

            if (isset($status->find('span')[0])) {
                $status2 = $status->find('span')[0]->text();
            }
        } else {
            $status1 = 'Status not available';
            $status2 = null;
        }

        $data[$i]['status1'] = $status1;
        $data[$i]['status2'] = trim($status2);

        $data[$i]['details_page_url'] = 'https://search.ipaustralia.gov.au' . explode('?', $item->getAttribute('data-markurl'))[0];
        $i++;
    }
}

print_r($data);

//$a = $dom->find('a')[0];
//echo $a->text; // "click here"